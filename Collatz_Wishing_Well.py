import svgwrite
import math
import sympy

center = (1600, 1600)
canvas_size = (2*center[0], 2*center[1])

canvas = svgwrite.Drawing('Collatz_Wishing_Well.svg', profile='tiny', size=canvas_size)
canvas.add(canvas.text('Collatz Wishing Well', insert=(5, 15), fill='green'))

class CollatzMetrics:
    n = 0
    n_modulo_six = 0
    times_visited = 0 #philosophical question, do you included starting/test points (I'd argue no)
    times_visited_by_odd_path = 0
    cycle_exits = 0
    
    def __init__(self, n):
        self.n = n
        self.n_modulo_six = n % 6

visited_nodes = {}
collatz_entry_points = {4,2,1,0,-1,-2,-5,-7,-10,-14,-17,-20,-25,-34,-37,-41,-50,-55,-61,-68,-74,-82,-91,-110,-122,-136,-164,-182,-272}

"""
-2 0.3202/0.0016 ~= 200.125 ~= 200 = (7^2+1)*4
-14 0.2701/0.0016 ~= 168.8125 ~= 13^2 - epsilon
-20 0.0584/0.0016 ~= 36.5 ~= 6^2 + epsilon
-50 0.0572/0.0016 ~= 35.75 ~= 6^2 - epsilon
-68 0.0462/0.0016 ~= 28.875 ~= 5^2 + epsilon
-74 0.0348/0.0016 ~= 21.75 ~= 5^2 - epsilon
-110 0.0073/0.0016 ~= 4.5625 ~= 4^2 + epsilon
-122 0.1947/0.0016 ~= 121.6875 ~= 11^2 + epsilon
-164 0.0033/0.0016 ~= 2 ~= 2 = (7+1)/4
-182 0.0062/0.0016 ~= 3.875 ~= 4^2 - epsilon
-272 0.0016/0.0016 = 1 // this feels sort of lucky, but I guess there were 16/10,000 (so the sample size is okay-ish)

"""

def reset():
    visited_nodes = {}

def step(n):
    if n % 2 == 0:
        return n//2
    else:
        return 3*n + 1

def augmented_step(n):
    was_even = (n % 2 == 0)
    n = step(n)
    if not n in visited_nodes.keys():
        visited_nodes[n] = CollatzMetrics(n) # first node is not added to dictionary (i.e. test_value)
    metrics = visited_nodes[n]
    metrics.times_visited += 1
    # you can do some 
    if not was_even:
        metrics.times_visited_by_odd_path += 1
    return n
    
def simulate(n):
    while (n not in collatz_entry_points):
        n = augmented_step(n)
    if not n in visited_nodes.keys():
        visited_nodes[n] = CollatzMetrics(n) # first node is not added to dictionary (i.e. test_value)
    metrics = visited_nodes[n]
    metrics.cycle_exits += 1
    
def exit_node(n):
    while (n not in collatz_entry_points):
        n = step(n)
    return n

def print_node(number, radius, angle, color):
    position = (center[0] + 20*radius*math.cos(angle), center[1] + 20*radius*math.sin(angle))
    text_position = (position[0] - 10, position[1] + 5)
    if abs(position[0] - center[0]) > center[0]:
        return
    if abs(position[1] - center[1]) > center[1]:
        return
    line_color = 'pink' if sympy.isprime(number) else 'white'

    canvas.add(canvas.circle(center=position, r=10, fill=color, stroke=line_color, stroke_width=3))
    canvas.add(canvas.text(number, insert=text_position, fill='black'))
    if number == 151:
        print(151, angle % (2*math.pi))
    if number == 475:
        print(475, angle % (2*math.pi))

colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple']
def print_node_basic(number):
    radius = math.sqrt(number)/math.sqrt(math.pi)
    angle = number
    color = colors[number % 6]
    print_node(number, radius, angle, color)
    
def print_node_manual(number, angle):
    radius = math.sqrt(number)/math.sqrt(math.pi)
    color = colors[number % 6]
    print_node(number, radius, angle, color)
    
def print_node_manual(number, angle, radius):
    color = colors[number % 6]
    print_node(number, radius, angle, color)

def lerp(a, b, t):
    return a + (b-a) * t
    
def print_tree(node=8,min_probability=0, max_probability=1):
    # base case
    if not node in visited_nodes:
        return
    
    # rendering step
    radius = 2.5*node**((2/3)**0)/(math.pi)**(2/3)
    print_node_manual(node, 2*math.pi*min_probability, radius)
    
    # recursive cases (tree traversal)
    if node % 6 == 4:
        if (node - 1)//3 % 3 == 0:
            print_tree((node - 1)//3, min_probability, min_probability) # epsilon path 3*2^k
            print_tree(2*node, min_probability, max_probability)
        else:
            odd_probability = visited_nodes[node].times_visited_by_odd_path/visited_nodes[node].times_visited
            mid_probability = lerp(min_probability, max_probability, 1 - odd_probability)
            print_tree((node - 1)//3, min_probability, mid_probability)
            print_tree(2*node, mid_probability, max_probability)
    else:
        print_tree(2*node, min_probability, max_probability)

"""
from random import randrange
#min_starting_value = 16 + 1
#max_starting_value = int(2**128) - 1
min_starting_value = -int(2**128)
max_starting_value = -1
test_cases = 10000
for test_case in range(test_cases):
    test_value = randrange(min_starting_value, max_starting_value)
    simulate(test_value)
"""

#for i in range(100):
#    print_node_basic(i)

"""
for n in visited_nodes.values():
    if n.cycle_exits > 0:
        print(n.n, float(n.cycle_exits/test_cases))
"""

for i in range(1000):
    radius = 2*math.sqrt(i)/math.sqrt(math.pi)
    angle = 2*math.pi*math.sqrt(i)
    exit = exit_node(-i)
    color = 'white' #essentially covers "-2" case
    if exit == -14:
        color = svgwrite.rgb(25, 25, 25, '%') # 'light gray' # lol, I just realized this should be 'dark gray' and the other is 'light gray'
    elif exit == -20:
        color = svgwrite.rgb(50, 50, 50, '%') # 'dark gray'
    elif exit == -50:
        color = 'red'
    elif exit == -68:
        color = 'orange'
    elif exit == -74:
        color = 'yellow'
    elif exit == -110:
        color = 'green'
    elif exit == -122:
        color = 'blue'
    elif exit == -164:
        color = 'indigo'
    elif exit == -182:
        color = 'violet'
    elif exit == -272:
        color = 'pink'
        
    print_node(i, radius, angle, color)

#print_tree()

canvas.save()
