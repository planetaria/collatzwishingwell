using System.Numerics;
using System.Collections.Generic;

public static class CollatzSequence
{
	private static Dictionary<BigInt, CollatzMetrics> visited_nodes = new Dictionary<BigInt, CollatzMetrics>();
	private static BigInt six = new BigInt(6);
	
	public static void reset()
	{
		visited_nodes = new Dictionary<BigInt, CollatzMetrics>();
	}
	
	public static void simulate(BigInt n)
	{
		while (!n.IsOne)
		{
			n = augmented_step(n)
		}
	}
	
	private static BigInt augmented_step(BigInt n)
	{
		bool was_even = n.IsEven;
		n = step(n);
		if (!visited_nodes.ContainsKey(n))
		{
			int remainder = (int) BigInteger.Remainder(n, six); 
			visited_nodes[n] = new CollatzMetrics(){modulo_of_six = remainder};
		}
		CollatzMetrics metrics = visited_nodes[n];
		metrics.times_visited += 1;
		if (!was_even)
		{
			metrics.times_visited_by_odd_path += 1;
		}
	}
	
	private static BigInt step(BigInt n)
	{
		if (n.IsEven)
		{
			return n/2;
		}
		else
		{
			return 3*n + 1
		}
	}
}
