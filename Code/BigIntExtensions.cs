using System.Numerics;

public static class BigIntExtensions
{
	// I am willing to pay for the extra cost for the sake of marginally better statistics.
	static RNGCryptoServiceProvider random_number_generator = new RNGCryptoServiceProvider();
	// static Random random_number_generator = new Random();

	public static BigInt random(BigInt max)
	{
		BigInteger result;
		byte[] bytes = new byte[max.GetByteCount(true)];
		while(true)
		{
			random_number_generator.GetBytes(bytes);
			BigInteger p = new BigInteger(bytes);
			if (result <= max)
			{
				break;
			}
		}
		return result
	}
	
	public static BigInt random(BigInt min, BigInt max)
	{
		BigInteger random_range = max - min;
		BigInteger unshifted_result = random(random_range);
		return unshifted_result + min;
	}
}
